package com.hp.brownbag.security.server;

import javax.jws.WebService;

/**
 * @author Christophe Weyn
 */
@WebService(endpointInterface = "com.hp.brownbag.security.server.BrownBagService",
            targetNamespace = "http://security.brownbag.hp.com",
            portName = "BrowBagServicePort", serviceName = "BrownBagService"
)
public class BrownBagServiceImpl implements BrownBagService {


	@Override
	public String sayHello() {

        System.out.println("Client connected");
		return "It's feeding time!";
	}
}