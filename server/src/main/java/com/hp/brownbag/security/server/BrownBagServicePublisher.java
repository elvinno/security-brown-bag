package com.hp.brownbag.security.server;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.ws.Endpoint;
import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.SecureRandom;

/**
 * Run this file to create a server.
 * Adjust your IP hosts file so server.security.brownbag.hp.com points to the IP address where this server is running
 * e.g. localhost or 127.0.0.1
 * <br/>
 * Point your browser to
 * http://server.security.brownbag.hp.com:8080/BrownBagService
 * or
 * https://server.security.brownbag.hp.com:8443/BrownBagService
 *
 * @author Christophe Weyn
 */
public class BrownBagServicePublisher {

	private static final String HOSTNAME = "server.security.brownbag.hp.com";

	private static final String KEY_STORE = "certificates/server/keystore.jks";
	private static final String TRUST_STORE = "certificates/server/truststore.jks";
	private static final String KEY_PASS = "changeit";


	public static void main(String[] args) throws Exception {

        System.setProperty("javax.net.debug", "ssl");

		System.out.println("Publish the service");
		new BrownBagServicePublisher().startServers();
	}

	/**
	 * Starts 2 servers.
	 * <br/>
	 * One that listens to the HTTP protocol, not secured and one on the HTTPS protocol.
	 */
	private void startServers() {

		new Thread(new BrownBagHttpServer()).start();
		new Thread(new BrownBagHttpsServer()).start();
	}

	class BrownBagHttpServer implements Runnable {

		@Override
		public void run() {

			final String httpEndPointAddress = "http://" + HOSTNAME + ":8080/";
			Endpoint.publish(httpEndPointAddress, new BrownBagServiceImpl());
		}
	}

	class BrownBagHttpsServer implements Runnable {

		@Override
		public void run() {
			try {
				this.createHttps();
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		private void createHttps() throws Exception {
			//Load the keystore
			KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(new FileInputStream(KEY_STORE), KEY_PASS.toCharArray());
			keyFactory.init(keyStore, KEY_PASS.toCharArray());

			//Load the truststore
			TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			KeyStore trustStore = KeyStore.getInstance("JKS");
			trustStore.load(new FileInputStream(TRUST_STORE), KEY_PASS.toCharArray());
			trustFactory.init(trustStore);

			//Create the SSL context with keystore & truststore
			SSLContext ssl = SSLContext.getInstance("TLS");
			ssl.init(keyFactory.getKeyManagers(), trustFactory.getTrustManagers(), new SecureRandom());

			HttpsServer httpsServer = HttpsServer.create(new InetSocketAddress(HOSTNAME, 8443), 10);
			httpsServer.setHttpsConfigurator(new HttpsConfigurator(ssl) {

				public void configure(HttpsParameters params) {

					//require client authentication
					SSLParameters sslparams = getSSLContext().getDefaultSSLParameters();
					sslparams.setNeedClientAuth(true);
					params.setSSLParameters(sslparams);
				}
			});

			httpsServer.start();

			Endpoint endpoint = Endpoint.create(new BrownBagServiceImpl());

			endpoint.publish(httpsServer.createContext("/BrownBagService"));
		}
	}

}