package com.hp.brownbag.security.server;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

/**
 *
 * DNS name: com.hp.brownbag.security.server
 *
 * server.security.brownbag.hp.com
 *
 * @author Christophe Weyn
 */
@WebService(name = "BrownBagService",
            targetNamespace = "http://security.brownbag.hp.com",
            wsdlLocation = "http://server.security.brownbag.hp.com:8443/BrownBagService?wsdl"
)
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public interface BrownBagService {


	@WebMethod(operationName = "helloBrownBag")
	@WebResult(name="brownbag")
	String sayHello();
}