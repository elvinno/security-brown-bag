HP Brownbag mutual SSL in Java.
This application uses the SOAP protocol over a SSL secured connection.

Running:

 1. Add "server.security.brownbag.hp.com" to your IP hosts file. Point it to the IP you're going to run the server e.g. 127.0.0.1

 2. Create a server keystore use "server.security.brownbag.hp.com" as Common Name. This has to be the same as the URI you're connecting to; SSL checks this.
    keytool -genkey -keyalg RSA -alias sec_server -keystore server_keystore.jks -storepass changeit -validity 360 -keysize 2048

 3. Extract server certificate from server keystore
    keytool -export -keystore server_keystore.jks -alias sec_server -file sec_server.crt -storepass changeit

 4. Create client truststore with server certificate
    keytool -import -file sec_server.crt -alias sec_server -keystore client_truststore.jks -storepass changeit

 5. Create client keystore
    keytool -genkey -keyalg RSA -alias sec_client -keystore client_keystore.jks -storepass changeit -validity 360 -keysize 2048

 6. Extract client certificate from client keystore
    keytool -export -keystore client_keystore.jks -alias sec_client -file sec_client.crt -storepass changeit

 7. Import client certificate in server truststore
    keytool -import -file sec_client.crt -alias sec_client -keystore server_truststore.jks -storepass changeit

 8. Run main method in BrownBagServicePublisher.java

 9. Open http://server.security.brownbag.hp.com:8080/BrownBagService or https://server.security.brownbag.hp.com:8443/BrownBagService in browser.
    The https site requires import of the client certificate via PKCS12 file
    keytool -importkeystore -srckeystore client_keystore.jks -destkeystore client_keystore.p12 -srcstoretype JKS -deststoretype PKCS12 -srcstorepass changeit -deststorepass changeit -srcalias sec_client -destalias sec_client -srckeypass changeit -destkeypass changeit -noprompt

10. Run main method in Client.java

Remark:

The keystore & truststore for the client and server both have the same name. These are 2 different files. In order for the application to work.
The files have to put in different directories are have to be given different names.
The are already pre configured files in the certificates directory.




