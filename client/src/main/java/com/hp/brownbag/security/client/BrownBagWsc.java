package com.hp.brownbag.security.client;

/**
 * @author Christophe Weyn
 */
public interface BrownBagWsc {

	void receiveHelloFromServer();
}
