package com.hp.brownbag.security.client;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.xml.transform.StringSource;

import javax.xml.transform.stream.StreamResult;

/**
 * @author Christophe Weyn
 */
@Controller
public class BrownBagWscImpl implements BrownBagWsc {

	@Autowired
	private WebServiceTemplate wsTemplate;

	@Override
	public void receiveHelloFromServer() {

        final String xml = "<bb:helloBrownBag xmlns:bb=\"http://security.brownbag.hp.com\">></bb:helloBrownBag>";

        wsTemplate.sendSourceAndReceiveToResult(new StringSource(xml), new StreamResult(System.out));
	}
}
