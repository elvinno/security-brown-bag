package com.hp.brownbag.security.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * @author Christophe Weyn
 */
public class Client implements Runnable {

	private final static String KEYSTORE_PASS = "changeit";
	private final static String KEYSTORE = "certificates/client/keystore.jks";
//	private final static String KEYSTORE = "certificates/client/empty_keystore.jks";
	private final static String TRUSTSTORE = "certificates/client/truststore.jks";

	@Autowired
	private BrownBagWsc brownBagWsc;

	private static void setupSslContext() throws NoSuchAlgorithmException, KeyStoreException, IOException, CertificateException, UnrecoverableKeyException, KeyManagementException {

		//Load the keystore
		KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		KeyStore keyStore = KeyStore.getInstance("JKS");
		keyStore.load(new FileInputStream(KEYSTORE), KEYSTORE_PASS.toCharArray());
		keyFactory.init(keyStore, KEYSTORE_PASS.toCharArray());

		//Load the truststore
		TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		KeyStore trustStore = KeyStore.getInstance("JKS");
		trustStore.load(new FileInputStream(TRUSTSTORE), KEYSTORE_PASS.toCharArray());
		trustFactory.init(trustStore);

		//Configure SSL Context for the JVM
		SSLContext sslContext = SSLContext.getInstance("SSL");
		sslContext.init(keyFactory.getKeyManagers(), trustFactory.getTrustManagers(), null);
		SSLContext.setDefault(sslContext);
	}

	public static void main(String args[]) throws Exception {

        System.setProperty("javax.net.debug", "ssl");

        setupSslContext();

		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("spring/spring.ctx.xml");

		Client client = new Client();
		context.getBeanFactory().autowireBeanProperties(client, AutowireCapableBeanFactory.AUTOWIRE_NO, false);

		client.run();
	}

	@Override
	public void run() {

		brownBagWsc.receiveHelloFromServer();
	}
}
